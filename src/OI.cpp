#include <Commands/ClawOpenCmd.h>
#include <Commands/ClawCloseCmd.h>
#include <Commands/ArmOutCmd.h>
#include <Commands/ArmInCmd.h>
#include <Commands/WinchArmCmd.h>
#include <Commands/PickupToggleCmd.h>
#include <Commands/BallEgressToggleCmd.h>
#include "OI.h"
#include "RobotMap.h"

OI::OI() : m_joy(0),
//	m_claw_close_button(&m_joy, CLAW_CLOSE_BUTTON),
//	m_claw_open_button(&m_joy, CLAW_OPEN_BUTTON),
//	m_arm_out_button(&m_joy, ARM_OUT_BUTTON),
//	m_arm_in_button(&m_joy, ARM_IN_BUTTON),
	m_winch_arm_button(&m_joy, WINCH_ARM_BUTTON),
	m_pickup_toggle_button(&m_joy, PICKUP_TOGGLE_BUTTON),
	m_ball_egress_toggle_button(&m_joy, BALL_EGRESS_TOGGLE_BUTTON),
	m_auto_x(0.0), m_auto_y(0.0), m_auto_z(0.0)
{
   // Connect the buttons to commands
//   m_claw_close_button.WhileHeld(new ClawCloseCmd());
//   m_claw_open_button.WhileHeld(new ClawOpenCmd());
//   m_arm_out_button.WhileHeld(new ArmOutCmd());
//   m_arm_in_button.WhileHeld(new ArmInCmd());
   m_winch_arm_button.WhenPressed(new WinchArmCmd());
   m_pickup_toggle_button.WhenPressed(new PickupToggleCmd());
   m_ball_egress_toggle_button.WhenPressed(new BallEgressToggleCmd());

   SmartDashboard::PutData("OpenClaw", new ClawOpenCmd());
}

Joystick& OI::get_joystick()
{
	return (m_joy);
}

void OI::put_auto_values(const float in_x, const float in_y, const float in_z)
{
	m_auto_x = in_x;
	m_auto_y = in_y;
	m_auto_z = in_z;
}

float OI::get_auto_x()
{
	return (m_auto_x);
}

float OI::get_auto_y()
{
	return (m_auto_y);
}

float OI::get_auto_z()
{
	return (m_auto_z);
}
