#ifndef COMMAND_BASE_H
#define COMMAND_BASE_H

#include <Subsystems/ArmSystem.h>
#include <Subsystems/ClawSystem.h>
#include <Subsystems/DriveSystem.h>
#include <Subsystems/WinchSystem.h>
#include <Subsystems/PickupSystem.h>
#include <Subsystems/BallEgressSystem.h>
#include <Subsystems/UltrasonicSensor.h>
#include <string>
#include "Commands/Command.h"
#include "OI.h"
#include "WPILib.h"

/**
 * The base for all commands. All atomic commands should subclass CommandBase.
 * CommandBase stores creates and stores each control system. To access a
 * subsystem elsewhere in your code in your code use CommandBase.examplesubsystem
 */
class CommandBase: public Command
{
public:
	CommandBase(const std::string &name);
	CommandBase();
	static void init();
	// Create a single static instance of all of your subsystems
	static ArmSystem *armSystem;
	static ClawSystem *clawSystem;
	static DriveSystem *driveSystem;
	static WinchSystem *winchSystem;
	static PickupSystem *pickupSystem;
	static BallEgressSystem *ballEgressSystem;
	static OI *oi;
	static UltrasonicSensor *ultraSonicSensor;
};

#endif
