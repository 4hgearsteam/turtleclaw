#include "TeleopGrp.h"
#include "DriveCmd.h"
#include "WinchPullCmd.h"
#include "WPILib.h"

TeleopGrp::TeleopGrp() {
	// Add Commands here:
	// e.g. AddSequential(new Command1());
	//      AddSequential(new Command2());
	// these will run in order.

	// To run multiple commands at the same time,
	// use AddParallel()
	// e.g. AddParallel(new Command1());
	//      AddSequential(new Command2());
	// Command1 and Command2 will run in parallel.

	// A command group will require all of the subsystems that each member
	// would require.
	// e.g. if Command1 requires chassis, and Command2 requires arm,
	// a CommandGroup containing them would require both the chassis and the
	// arm.

	/*Gets output from Ultrasonic sensor*/
	AnalogInput *ai;
	ai = new AnalogInput(0);
	int aivalue = ai->GetValue();
	SmartDashboard::PutNumber("AnalogInput" , aivalue);

	AddParallel(new WinchPullCmd());
	AddParallel(new DriveCmd(1000.0, DriveCmd::DRIVE_CTRL_JOYSTICK, 0.0, 0.0, 0.0, "tele"));

}
