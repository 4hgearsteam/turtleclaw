#include "AutonomousGrp.h"
#include "DriveCmd.h"

// #define STATIC_FORWORD  DriveCmd::DRIVE_CTRL_STATIC,0.0,-0.5,0.0


#define AUTO_FORWARD(time, forward) AddSequential(new DriveCmd(time, DriveCmd::DRIVE_CTRL_STATIC, 0.0, -forward, 0.0,"forward"));
#define AUTO_BACKWARD(time, backward) AddSequential(new DriveCmd(time, DriveCmd::DRIVE_CTRL_STATIC, 0.0, backward, 0.0,"backward"));
#define AUTO_TURN_RIGHT(time, turn_right) AddSequential(new DriveCmd(time, DriveCmd::DRIVE_CTRL_STATIC, 0.0, 0.0, -turn_right,"turn right"));
#define AUTO_TURN_LEFT(time, turn_left) AddSequential(new DriveCmd(time, DriveCmd::DRIVE_CTRL_STATIC, 0.0, 0.0, turn_left,"turn left"));
#define AUTO_STRAFE_RIGHT(time, strafe_right) AddSequential(new DriveCmd(time, DriveCmd::DRIVE_CTRL_STATIC, -strafe_right, 0.0, 0.0,"strafe right"));
#define AUTO_STRAFE_LEFT(time, strafe_left) AddSequential(new DriveCmd(time, DriveCmd::DRIVE_CTRL_STATIC, strafe_left, 0.0, 0.0,"strafe left"));


#define SLOW	0.3
#define MEDIUM  0.5
#define FAST	0.7

AutonomousGrp::AutonomousGrp() {

	// Add Commands here:
	// e.g. AddSequential(new Command1());
	//      AddSequential(new Command2());
	// these will run in order.

	// To run multiple commands at the same time,
	// use AddParallel()
	// e.g. AddParallel(new Command1());
	//      AddSequential(new Command2());
	// Command1 and Command2 will run in parallel.

	// A command group will require all of the subsystems that each member
	// would require.
	// e.g. if Command1 requires chassis, and Command2 requires arm,
	// a CommandGroup containing them would require both the chassis and the
	// arm.

	// AddSequential(new DriveCmd(2.0, STATIC_FORWARD, "forward");

	AUTO_FORWARD(2.0, SLOW);
	AUTO_BACKWARD(2.0, MEDIUM);

	// AUTO_TURN_RIGHT(2.0, SLOW);
	// AUTO_TURN_LEFT(2.0, FAST);
	// AUTO_STRAFE_RIGHT(2.0, MEDIUM);
	// AUTO_STRAFE_LEFT(2.0, MEDIUM);

//	AddSequential(new DriveCmd(2.0, DriveCmd::DRIVE_CTRL_STATIC, 0.0, -0.3, 0.0, "forward") );
//	AddSequential(new DriveCmd(2.0, DriveCmd::DRIVE_CTRL_STATIC, 0.0, 0.3, 0.0, "backward") );

//	AddSequential(new DriveCmd(2.0, DriveCmd::DRIVE_CTRL_STATIC, 0.0, 0.0, 0.3, "turn right") );
//	AddSequential(new DriveCmd(2.0, DriveCmd::DRIVE_CTRL_STATIC, 0.0, 0.0, -0.3, "turn left") );
}
