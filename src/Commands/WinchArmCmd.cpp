#include "WinchArmCmd.h"
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"

WinchArmCmd::WinchArmCmd() {
	// Use Requires() here to declare subsystem dependencies
	Requires(winchSystem);
}

// Called just before this Command runs the first time
void WinchArmCmd::Initialize() {

}

// Called repeatedly when this Command is scheduled to run
void WinchArmCmd::Execute() {
	winchSystem->Arm();
}

// Make this return true when this Command no longer needs to run execute()
bool WinchArmCmd::IsFinished() {
	return true;
}

// Called once after isFinished returns true
void WinchArmCmd::End() {

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void WinchArmCmd::Interrupted() {

}
