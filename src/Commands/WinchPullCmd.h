#ifndef WinchPullCmd_H
#define WinchPullCmd_H

#include "../CommandBase.h"
#include "WPILib.h"

const float WINCH_PULL_CMD_TIMEOUT  = 30.0;

class WinchPullCmd : public CommandBase {
public:
	WinchPullCmd();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	float CommandTimeout;
};

#endif  // WinchPullCmd_H
