#include "BallEgressToggleCmd.h"
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"

BallEgressToggleCmd::BallEgressToggleCmd() {
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(Robot::chassis.get());
}

// Called just before this Command runs the first time
void BallEgressToggleCmd::Initialize() {

}

// Called repeatedly when this Command is scheduled to run
void BallEgressToggleCmd::Execute() {
	ballEgressSystem->Toggle();
}

// Make this return true when this Command no longer needs to run execute()
bool BallEgressToggleCmd::IsFinished() {
	return true;
}

// Called once after isFinished returns true
void BallEgressToggleCmd::End() {

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void BallEgressToggleCmd::Interrupted() {

}
