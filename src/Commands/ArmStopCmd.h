#ifndef ArmStopCmd_H
#define ArmStopCmd_H

#include "../CommandBase.h"
#include "WPILib.h"

const float ARM_STOP_CMD_TIMEOUT  = 0.0;   // No timeout

class ArmStopCmd: public CommandBase
{
public:
	ArmStopCmd();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	float CommandTimeout;
};

#endif
