#ifndef DriveCommand_H
#define DriveCommand_H

#include "../CommandBase.h"
#include "WPILib.h"

const float DRIVE_CMD_TIMEOUT = 0.0;  // No timeout

class DriveCmd: public CommandBase
{
public:
	enum DriveSource {
		DRIVE_CTRL_JOYSTICK,
		DRIVE_CTRL_STATIC,
		DRIVE_CTRL_AUTO,
	};

	DriveCmd(float in_timeout, DriveSource in_source, float in_x, float in_y, float in_z, std::string name);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	float CommandTimeout;
	DriveSource inputSource;
	float static_x;
	float static_y;
	float static_z;
	std::string cmd_name;
};

#endif
