#include "ClawStopCmd.h"
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"

ClawStopCmd::ClawStopCmd() : CommandBase("ClawStopCmd"),  CommandTimeout(CLAW_STOP_CMD_TIMEOUT)
{
	// Use Requires() here to declare subsystem dependencies
	Requires(clawSystem);
	SetTimeout(CommandTimeout);
}

// Called just before this Command runs the first time
void ClawStopCmd::Initialize()
{

}

// Called repeatedly when this Command is scheduled to run
void ClawStopCmd::Execute()
{
	clawSystem->Stop();
}

// Make this return true when this Command no longer needs to run execute()
bool ClawStopCmd::IsFinished()
{
	return false;
}

// Called once after isFinished returns true
void ClawStopCmd::End()
{
	clawSystem->Stop();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void ClawStopCmd::Interrupted()
{
	clawSystem->Stop();
}
