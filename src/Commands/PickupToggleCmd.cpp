#include "PickupToggleCmd.h"
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"

PickupToggleCmd::PickupToggleCmd() {
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(Robot::chassis.get());
}

// Called just before this Command runs the first time
void PickupToggleCmd::Initialize() {

}

// Called repeatedly when this Command is scheduled to run
void PickupToggleCmd::Execute() {
	pickupSystem->Toggle();
}

// Make this return true when this Command no longer needs to run execute()
bool PickupToggleCmd::IsFinished() {
	return true;
}

// Called once after isFinished returns true
void PickupToggleCmd::End() {

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void PickupToggleCmd::Interrupted() {

}
