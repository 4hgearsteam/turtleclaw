#include <Commands/DriveCmd.h>
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"


DriveCmd::DriveCmd(float in_timeout, DriveSource in_source, float in_x, float in_y, float in_z, std::string in_name) :
						CommandBase("DriveCmd"), CommandTimeout(in_timeout), inputSource(in_source),
						static_x(in_x), static_y(in_y), static_z(in_z), cmd_name(in_name)
{
	Requires(driveSystem);
	if (CommandTimeout != 0){
		SetTimeout(CommandTimeout);
	}
}

// Called just before this Command runs the first time
void DriveCmd::Initialize()
{

}

// Called repeatedly when this Command is scheduled to run
void DriveCmd::Execute()
{
    float new_x = 0.0;
    float new_y = 0.0;
    float new_z = 0.0;
	Joystick& joystick = CommandBase::oi->get_joystick();
	static unsigned int count=0;
	SmartDashboard::PutNumber("count",count++);
	SmartDashboard::PutString("drivesource", cmd_name.c_str());
	switch (inputSource) {
	case DRIVE_CTRL_JOYSTICK:
		new_x = joystick.GetRawAxis(LEFT_STICK_X_AXIS);
		new_y = joystick.GetRawAxis(LEFT_STICK_Y_AXIS);
		new_z = joystick.GetRawAxis(RIGHT_STICK_X_AXIS);
		break;
	case DRIVE_CTRL_STATIC:
	    new_x = static_x;
	    new_y = static_y;
	    new_z = static_z;
		break;
	case DRIVE_CTRL_AUTO:
		new_x = CommandBase::oi->get_auto_x();
		new_y = CommandBase::oi->get_auto_y();
		new_z = CommandBase::oi->get_auto_z();
		break;

	default:
		std::cout << "ERROR: unknown drive direction source" << std::endl;
		break;

	}

	driveSystem->Drive(new_x, new_y, new_z);
}

// Make this return true when this Command no longer needs to run execute()
bool DriveCmd::IsFinished()
{
	if (IsTimedOut())
	{
		return true;
	}
	else
	{
		return false;
	}

}

// Called once after isFinished returns true
void DriveCmd::End()
{
	driveSystem->Stop();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void DriveCmd::Interrupted()
{
	driveSystem->Stop();
}
