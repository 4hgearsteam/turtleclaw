#ifndef ArmInCmd_H
#define ArmInCmd_H

#include "../CommandBase.h"
#include "WPILib.h"

const float ARM_IN_CMD_TIMEOUT  = 2.0;

class ArmInCmd: public CommandBase
{
public:
	ArmInCmd();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	float CommandTimeout;
};

#endif
