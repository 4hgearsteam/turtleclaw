#ifndef ArmOutCmd_H
#define ArmOutCmd_H

#include "../CommandBase.h"
#include "WPILib.h"

const float ARM_OUT_CMD_TIMEOUT  = 2.0;

class ArmOutCmd: public CommandBase
{
public:
	ArmOutCmd();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	float CommandTimeout;
};

#endif
