#ifndef BallEgressToggleCmd_H
#define BallEgressToggleCmd_H

#include "../CommandBase.h"

const double egress_power_on = 1.0;
const double egress_power_off = 0.0;

class BallEgressToggleCmd : public CommandBase {
public:
	BallEgressToggleCmd();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif  // BallEgressToggleCmd_H
