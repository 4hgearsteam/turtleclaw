#ifndef PickupToggleCmd_H
#define PickupToggleCmd_H

#include "../CommandBase.h"

class PickupToggleCmd : public CommandBase {
public:
	PickupToggleCmd();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif  // PickupToggleCmd_H
