#ifndef BallEgressSystem_H
#define BallEgressSystem_H

#include <Commands/Subsystem.h>
#include "WPILib.h"

const double ball_egress_power_on = 1.0;
const double ball_egress_power_off = 0.0;


class BallEgressSystem : public Subsystem {
private:
	// It's desirable that everything possible under private except
	// for methods that implement subsystem capabilities
	VictorSP m_motor;
	bool m_active;

public:
	BallEgressSystem();
	void InitDefaultCommand();
	void Toggle(void);
	void On(void);
	void Off(void);
};

#endif  // BallEgressSystem_H
