#include "UltrasonicSensor.h"
#include "../RobotMap.h"
#include "WPILib.h"

UltrasonicSensor::UltrasonicSensor() : Subsystem("UltrasonicSensor"), ai(0)
{


}

void UltrasonicSensor::InitDefaultCommand() {
	// Set the default command for a subsystem here.
	// SetDefaultCommand(new MySpecialCommand());



}
void UltrasonicSensor::SendDistance()
{
	uint16_t aivalue;
	ai.SetOversampleBits(4);
	aivalue = ai.GetOversampleBits();
	SmartDashboard::PutNumber("AnalogInputOversampledBits" , aivalue);
	ai.SetAverageBits(2);
	aivalue = ai.GetValue();
	SmartDashboard::PutNumber("AnalogInputValue", aivalue);
	double distance = (double)aivalue * ai.GetAverageVoltage();
	SmartDashboard::PutNumber("Distance", distance);

}

// Put methods for controlling this subsystem
// here. Call these from Commands.
