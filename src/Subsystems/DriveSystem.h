#ifndef DriveSystem_H
#define DriveSystem_H

#include "Commands/Subsystem.h"
#include "WPILib.h"

class DriveSystem: public Subsystem
{
private:
	VictorSP m_motor_front_left;
	VictorSP m_motor_rear_left;
	VictorSP m_motor_front_right;
	VictorSP m_motor_rear_right;
	RobotDrive m_robot_drive;
public:
	DriveSystem();
	void InitDefaultCommand();
	void Drive(float axis_x, float axis_y, float axis_z);
	void Stop(void);
};

#endif
