#ifndef PickupSystem_H
#define PickupSystem_H

#include <Commands/Subsystem.h>
#include "WPILib.h"

//TODO set to half power to preserve motor
const double pickup_power_on = 1.0;
const double pickup_power_off = 0.0;

class PickupSystem : public Subsystem {
private:
	// It's desirable that everything possible under private except
	// for methods that implement subsystem capabilities
	VictorSP m_motor;
	bool m_active;

public:
	PickupSystem();
	void InitDefaultCommand();
	void Toggle(void);
	void On(void);
	void Off(void);
};

#endif  // PickupSystem_H
