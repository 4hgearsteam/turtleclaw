#include "BallEgressSystem.h"
#include "../RobotMap.h"

BallEgressSystem::BallEgressSystem() : Subsystem("ExampleSubsystem"), m_motor(MOTOR_BALL_EGRESS), m_active(false)  {

}

void BallEgressSystem::InitDefaultCommand() {
	// Set the default command for a subsystem here.
	// SetDefaultCommand(new MySpecialCommand());
	Off();
}

// Put methods for controlling this subsystem
// here. Call these from Commands.

void BallEgressSystem::Toggle() {
	if (m_active) {
		Off();
	}
	else {
		On();
	}
}


void BallEgressSystem::On() {
    m_active = true;
    m_motor.Set(ball_egress_power_on);
}



void BallEgressSystem::Off() {
    m_active = false;
    m_motor.Set(ball_egress_power_off);
}
