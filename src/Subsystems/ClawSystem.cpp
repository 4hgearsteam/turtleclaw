#include <Subsystems/ClawSystem.h>
#include <Commands/ClawStopCmd.h>
#include "../RobotMap.h"

ClawSystem::ClawSystem() :
		Subsystem("ExampleSubsystem"), m_doubleSolenoid(CLAW_PCM, CLAW_OPEN_CHANNEL, CLAW_CLOSE_CHANNEL)
{

}

void ClawSystem::InitDefaultCommand()
{
	// Set the default command for a subsystem here.
	SetDefaultCommand(new ClawStopCmd());
}

// Put methods for controlling this subsystem
// here. Call these from Commands.

void ClawSystem::Open(void)
{
	m_doubleSolenoid.Set(DoubleSolenoid::kForward);
}

void ClawSystem::Close(void)
{
	m_doubleSolenoid.Set(DoubleSolenoid::kReverse);
}

void ClawSystem::Stop()
{
	m_doubleSolenoid.Set(DoubleSolenoid::kOff);
}

float ClawSystem::GetPosition()
{
	return (0.5);
}
