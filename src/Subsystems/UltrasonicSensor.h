#ifndef UltrasonicSensor_H
#define UltrasonicSensor_H

#include <Commands/Subsystem.h>
#include "WPILib.h"

class UltrasonicSensor : public Subsystem {
private:
	AnalogInput ai;
	// It's desirable that everything possible under private except
	// for methods that implement subsystem capabilities

public:
	UltrasonicSensor();
	void InitDefaultCommand();
	void SendDistance();
};

#endif  // UltrasonicSensor_H
