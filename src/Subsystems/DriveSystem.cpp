#include <Commands/DriveCmd.h>
#include <Subsystems/DriveSystem.h>
#include "../RobotMap.h"

DriveSystem::DriveSystem() : Subsystem("DriveSystem"),
		m_motor_front_left(MOTOR_FRONT_LEFT_CHANNEL), m_motor_rear_left(MOTOR_REAR_LEFT_CHANNEL),
		m_motor_front_right(MOTOR_FRONT_RIGHT_CHANNEL), m_motor_rear_right(MOTOR_REAR_RIGHT_CHANNEL),
		m_robot_drive(m_motor_front_left, m_motor_rear_left, m_motor_front_right, m_motor_rear_right)
{
    m_motor_front_right.SetInverted(true);
    m_motor_rear_right.SetInverted(true);
}

void DriveSystem::InitDefaultCommand()
{
	// Set the default command for a subsystem here.
//	SetDefaultCommand(new DriveCmd());
}

// Put methods for controlling this subsystem
// here. Call these from Commands.

void DriveSystem::Drive(float axis_x, float axis_y, float axis_z)
{
	const int exp = 2;
	float new_axis_x = (axis_x < 0) ? -(pow(axis_x, exp)) : pow(axis_x,exp);
	float new_axis_y = (axis_y < 0) ? -(pow(axis_y, exp)) : pow(axis_y,exp);
	float new_axis_z = (axis_z < 0) ? -(pow(axis_z, exp)) : pow(axis_z,exp);

	SmartDashboard::PutNumber("x", axis_x);
	SmartDashboard::PutNumber("y", axis_y);
	SmartDashboard::PutNumber("z", axis_z);

	m_robot_drive.MecanumDrive_Cartesian(new_axis_x, new_axis_y, new_axis_z, 0.0);
	return;
}


void DriveSystem::Stop(void)
{
	m_robot_drive.StopMotor();
	return;
}
