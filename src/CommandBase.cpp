#include <Subsystems/ArmSystem.h>
#include <Subsystems/ClawSystem.h>
#include <Subsystems/DriveSystem.h>
#include <Subsystems/WinchSystem.h>
#include <Subsystems/UltrasonicSensor.h>
#include "CommandBase.h"
//#include "Commands/Scheduler.h"

// Initialize a single static instance of all of your subsystems to NULL
ArmSystem *CommandBase::armSystem = NULL;
ClawSystem *CommandBase::clawSystem = NULL;
DriveSystem *CommandBase::driveSystem = NULL;
WinchSystem *CommandBase::winchSystem = NULL;
PickupSystem *CommandBase::pickupSystem = NULL;
BallEgressSystem *CommandBase::ballEgressSystem = NULL;
UltrasonicSensor *CommandBase::ultraSonicSensor = NULL;
OI *CommandBase::oi = NULL;

CommandBase::CommandBase(const std::string &name) :
		Command(name)
{
}

CommandBase::CommandBase() :
		Command()
{

}

void CommandBase::init()
{
	oi = new OI();
	armSystem = new ArmSystem();
	clawSystem = new ClawSystem();
	driveSystem = new DriveSystem();
	winchSystem = new WinchSystem();
	pickupSystem = new PickupSystem();
	ballEgressSystem = new BallEgressSystem();
	ultraSonicSensor = new UltrasonicSensor();
}
