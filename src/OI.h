#ifndef OI_H
#define OI_H

#include "WPILib.h"

class OI
{
private:
	Joystick m_joy;
//	JoystickButton m_claw_close_button;
//	JoystickButton m_claw_open_button;
//	JoystickButton m_arm_out_button;
//	JoystickButton m_arm_in_button;
	JoystickButton m_winch_arm_button;
	JoystickButton m_pickup_toggle_button;
	JoystickButton m_ball_egress_toggle_button;
	float m_auto_x;
	float m_auto_y;
	float m_auto_z;

public:
	OI();
	Joystick& get_joystick();
	void put_auto_values(const float in_x, const float in_y, const float in_z);
	float get_auto_x();
	float get_auto_y();
	float get_auto_z();
};

#endif
