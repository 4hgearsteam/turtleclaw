#include "WPILib.h"
#include "Commands/Command.h"
#include "CommandBase.h"
#include "Commands/AutonomousGrp.h"
#include "Commands/TeleopGrp.h"
#include "Commands/DriveCmd.h"
#include "Robot.h"
#include "RobotMap.h"


void Robot::RobotInit()
{
	lw = LiveWindow::GetInstance();
	CommandBase::init();

	compressor_p = new Compressor(PRIMARY_PCM);
    compressor_p->SetClosedLoopControl(false);

    CameraServer::GetInstance()->StartAutomaticCapture(0); //Sends the camera to the SmartDashboard.
    CameraServer::GetInstance()->StartAutomaticCapture(1);

    autonomousCommand_p = new AutonomousGrp();
    teleopCommand_p = new TeleopGrp();
    CommandBase::ultraSonicSensor = new UltrasonicSensor();



}

/**
 * This function is called once each time the robot enters Disabled mode.
 * You can use it to reset any subsystem information you want to clear when
 * the robot is disabled.
 */
void Robot::DisabledInit()
{
	CommandBase::winchSystem->Disarm();
	CommandBase::pickupSystem->Off();
	CommandBase::ballEgressSystem->Off();
}

void Robot::DisabledPeriodic()
{
	Scheduler::GetInstance()->Run();
}

/**
 * This autonomous (along with the chooser code above) shows how to select between different autonomous modes
 * using the dashboard. The sendable chooser code works with the Java SmartDashboard. If you prefer the LabVIEW
 * Dashboard, remove all of the chooser code and uncomment the GetString code to get the auto name from the text box
 * below the Gyro
 *
 * You can add additional auto modes by adding additional commands to the chooser code above (like the commented example)
 * or additional comparisons to the if-else structure below with additional strings & commands.
 */
void Robot::AutonomousInit()
{
	/* std::string autoSelected = SmartDashboard::GetString("Auto Selector", "Default");
	if(autoSelected == "My Auto") {
		autonomousCommand.reset(new MyAutoCommand());
	} else {
		autonomousCommand.reset(new ExampleCommand());
	} */
	CommandBase::winchSystem->Disarm();
	CommandBase::pickupSystem->Off();
	CommandBase::ballEgressSystem->Off();
	autonomousCommand_p->Start();
}

void Robot::AutonomousPeriodic()
{
	Scheduler::GetInstance()->Run();
}

void Robot::TeleopInit()
{
	// This makes sure that the autonomous stops running when
	// teleop starts running. If you want the autonomous to
	// continue until interrupted by another command, remove
	// this line or comment it out.

	CommandBase::winchSystem->Disarm();
	CommandBase::pickupSystem->Off();
	CommandBase::ballEgressSystem->Off();

	if (autonomousCommand_p != NULL)
		autonomousCommand_p->Cancel();
	if (teleopCommand_p != NULL)
		teleopCommand_p->Start();
//	if (WinchPullCommand_p != NULL)
//	    WinchPullCommand_p->Start();
}

void Robot::TeleopPeriodic()
{
/*
// Test code for Ultrasonic sensor
	*/

	Scheduler::GetInstance()->Run();
	CommandBase::ultraSonicSensor->SendDistance();
}

void Robot::TestPeriodic()
{
	Scheduler::GetInstance()->Run();
}

START_ROBOT_CLASS(Robot)

