/*
 * Robot.h
 *
 *  Created on: Aug 14, 2016
 *      Author: Student
 */

#ifndef SRC_ROBOT_H_
#define SRC_ROBOT_H_

#include "WPILib.h"
#include "Commands/Command.h"
#include "Compressor.h"

#include "OI.h"

class Robot: public IterativeRobot {
public:

private:
	Compressor *compressor_p;
	Command *autonomousCommand_p;
	Command *teleopCommand_p;
	Command *WinchPullCommand_p;

	LiveWindow *lw;

	void RobotInit();
	void DisabledInit();
	void DisabledPeriodic();
	void AutonomousInit();
	void AutonomousPeriodic();
	void TeleopInit();
	void TeleopPeriodic();
	void TestPeriodic();
};


#endif /* SRC_ROBOT_H_ */
